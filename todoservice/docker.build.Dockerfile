FROM    microsoft/dotnet:2.2-sdk-alpine AS build
WORKDIR /src/todoservice
COPY    todoservice.csproj .
RUN     dotnet restore todoservice.csproj
COPY    . .
RUN     dotnet publish todoservice.csproj -c Release -o /app

FROM    microsoft/dotnet:2.2-aspnetcore-runtime AS runtime
WORKDIR /app
COPY    --from=build /app .
ENTRYPOINT ["dotnet", "todoservice.dll"]
