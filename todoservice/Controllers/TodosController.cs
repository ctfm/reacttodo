using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using todoservice.Models;
using todoservice.Services;

namespace todoservice.Controllers {
    
    [Route("api/[controller]")]
    [ApiController]
    public class TodosController : ControllerBase
    {
        private readonly TodoService TodoService;

        public TodosController(TodoService todoService)
        {
            TodoService = todoService;
        }

        [HttpGet]
        public ActionResult<List<Todo>> Get()
        {
            return TodoService.FindAll();
        }

        [HttpGet("{id:length(24)}", Name = "GetTodo")]
        public ActionResult<Todo> Get(string id)
        {
            var todo = TodoService.Find(id);
            if (todo == null) {
                return NotFound();
            }
            return todo;
        }

        [HttpPost]
        public ActionResult<Todo> Create(Todo todo)
        {
            TodoService.Create(todo);
            return CreatedAtRoute("GetTodo", new { id = todo.Id.ToString() }, todo);
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var todo = TodoService.Find(id);
            if (todo == null) {
                return NotFound();
            }
            TodoService.Delete(todo);
            return NoContent();
        }
    }

}
