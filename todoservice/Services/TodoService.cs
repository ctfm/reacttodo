using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using todoservice.Models;

namespace todoservice.Services {
    public class TodoService {
        public IMongoCollection<Todo> Todos {get;}
        private int MaxDocs {get;}

        public TodoService(IConfiguration config, int maxDocs = 10) {
            string conStr = config.GetSection("MongoDB:ConnectionString").Value;
            string dbName = config.GetSection("MongoDB:Database").Value;
            var client = new MongoClient(conStr);
            var db = client.GetDatabase(dbName);
            this.Todos = db.GetCollection<Todo>("Todos");
            this.MaxDocs = maxDocs;
        }

        public List<Todo> FindAll() {
            return this.Todos.Find(todo => true).ToList();
        }

        public Todo Find(string id) {
            var oid = new ObjectId(id);
            Todo todo = this.Todos.Find<Todo>(td => td.Id == oid).FirstOrDefault();
            return todo;
        }

        public Todo Create(Todo todo) {
            this.Todos.InsertOne(todo);
            bool ok = CleanUp();
            return todo;
        }

        public void Delete(Todo todo) {
            ObjectId id = todo.Id;
            this.Todos.DeleteOne(td => td.Id == id);
        }

        private bool CleanUp()  {
            //TODO
            try {
                long num = this.Todos.CountDocuments(t => true);
                if (num >= this.MaxDocs) {
                    int diff = (int)num - this.MaxDocs;
                    var results = this.Todos.Find(t => true).ToEnumerable()
                        .Take(diff)
                        .Select(td => this.Todos.DeleteOne(t => t.Id == td.Id));
                    bool ok = results.Sum(res => res.DeletedCount) == diff;
                    return ok;
                    //todo: var result = this.Todos.DeleteMany(t => ...);
                }
            }
            catch(Exception ex) {
                //todo
            }            
            return false;
        }
    }
}
