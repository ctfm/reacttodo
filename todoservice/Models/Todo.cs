using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace todoservice.Models {
    public class Todo {
        // [BsonId]
        public ObjectId Id {get; set;}
        public string CreatedAt { get; set; }
        public string Text { get; set; }
    }
}