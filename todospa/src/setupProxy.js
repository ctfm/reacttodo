const proxy = require('http-proxy-middleware');
//const apiUrl = 'https://192.168.99.100:5001';
const apiUrl = process.env.REACT_APP_API_URL || "http://localhost:5000";

module.exports = function(app) {
  app.use(proxy('/api', { 
    target: apiUrl,
    secure: false
  }));
};
