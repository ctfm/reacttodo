import React, { Component } from 'react';
import './TodoApp.css';

class TodoApp extends Component {
    constructor(props) {
      super(props);
      this.state = { items: [], text: '' };
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
      this.fetchAndSetTodos();
    }

    fetchAndSetTodos() {
      fetch("/api/todos")
        .then(response => response.json())
        .then(todos => this.setState((prev, props) => ({items: todos, text: ''})))
        .catch(reason => console.log("fetchAndSetTodos: " + reason));
    }

    postTodo(todo) {
      //TODO
      return fetch("/api/todos", {
        method: 'POST',
        body: JSON.stringify(todo),
        headers: {'Content-Type': 'application/json'}
      });
    }
  
    render() {
        return (
            <div>
                <header className="App-header">
                    <h3>TODO</h3>
                    <TodoList items={this.state.items} />
                    <form onSubmit={this.handleSubmit}>
                        <label htmlFor="new-todo">
                            What needs to be done?
                        </label>
                        <input
                            id="new-todo"
                            onChange={this.handleChange}
                            value={this.state.text}
                        />
                        <button>
                            Add #{this.state.items.length + 1}
                        </button>
                    </form>
                </header>
            </div>
        );
    }
  
    handleChange(e) {
      this.setState({ text: e.target.value });
    }
  
    handleSubmit(e) {
      e.preventDefault();
      if (!this.state.text.length) {
        return;
      }
      const newItem = {
        text: this.state.text,
        createdAt: new Date()
      };
      this.postTodo(newItem)
        .then(ok => this.fetchAndSetTodos())
        .catch(reason => console.log("handleSubmit: " + reason));
    }
  }
  
  class TodoList extends React.Component {
    render() {
      return (
        <ul>
          {this.props.items.map(item => (
            <li key={item.createdAt}>[{item.createdAt}] - {item.text}</li>
          ))}
        </ul>
      );
    }
  }

export default TodoApp;
  
//   ReactDOM.render(<TodoApp />, mountNode);